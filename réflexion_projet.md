# Travail de reflexion : 
## Première idée : Application de connection social afin de faire rencontrer des joueurs rôlistes.
    
    L'application a pour but de permettre à l'utilisateur de chercher autour de lui (Géolocalisation)
    Des joueurs souhaitant faire une partie de jeux de carte ou de jeux de rôles.

    Il y a la possibilité de choisir une multitude de jeux, par exemple carte magic, Donjon et dragon, jeux de sociétés ...
   
    L'application a pour but principal de créer des rendez-vous dans des lieux public, des bars, chez des particuliers. Mais surtout de créer des liens.

    Il y a la possibilité d'ajouter au compte utilisateur, un système de niveau de joueur (LVL), un historique des parties gagnés et perdu, créer un système d'affinité.

    Une problématique peut se poser, en effet imaginons que des joueurs, au bout de quelques partis ne trouvent plus l'utilité d'utiliser l'application,
    je pense palier à cela en ajoutant à l'utilisateur un système de récompenses.

    => Une gestion d'inventaire et d’équipement de l'utilisateur lui conférent des points de prestige.

    => J'ajoute la possibilité de gagner des récompenses pour soi ou à offrir à d'autres joueurs ce qui augmente les affinités.

    => Plus une affinité est élevé, plus les points d'expériences gagné entre ces deux joueurs est grande, cela incite les utilisateurs à rejouer encore.

    => J'imagine aussi des bonus lorsque l'on trouve une partie avec des nouveaux joueurs ce qui inciterais aussi les rencontres.

    L'idée est de créer une boucle de récompenses pour que l'application soit au cœur des parties.

    Pour le moment je vais me concentrer sur les bases de mon application. C'est à dire les menus principaux et les fonctionalités les plus importante.

    Une page de connexion et inscription

                        => formulaire de connexion ou d'inscription
                            =>login
                            => Mot de passe

    Page d'accueil  
                        => Nom du joueur
                        => Son lvl
                        => La carte en arriere plan
                            => Les futurs parties prévus dans la zone
                        => Menu burger
                                => rechercher joueurs 
                                => recherche partie
                                => amis
                                => créer partie
                                => agenda
                        => Un slide de bas en haut
                            =>liste des parties prévus dans la zone

    Page Créer partie
                        => logo et descriptions des jeux récemment joués
                            => Formulaire de création

    Page amis           =>liste des amis
                            => possibilité d'envoyer un message
                            => possibilité d'inviter dans une partie

    Page agenda         
                        => agenda et partie renseignés



## Technologies :
Quand on fait de la **POO** on aime séparer les actions, que chaque classe ait une mission particulière.
Grâce à des Frameworks comme Angular et Symfony, je vais pouvoir profiter de la gestion et de l'injection des dépendances.
Cela aura pour effet de bien structurer mon code. De plus Angular est parfait pour faire des **SPA**.

Je vais donc utiliser **Angular 15** pour le front et **Symfony 6** pour le back.

Ayant déjà travaillé sur **Symfony 5** lors de mon projet précèdent, je vais approfondir mes connaissances sur lui.
Je vais avoir besoin de faire des recheches et me former sur Angular.


Plus tard je vais travailler avec **API Platform**, qui me permetra d'envoyer des donnees en Json depuis symfony jusqu'à Angular.

Et je vais certainement en temps voulu appréhender **Docker**.

### Angular et les components :
C'est grâce au systeme de **components** que je vais pouvoir structurer mon travail sur le front en codant et stylisant brique par brique mon application.

![decoupage.png](decoupage.png)
# Maquettes, premieres idées :

![Connection.svg](Connection.svg)![Inscription.svg](Inscription.svg)

![Slide-off.svg](Slide-off.svg)![Slide.svg](Slide.svg)

![Burger.svg](Burger.svg)![Liste-amis.svg](Liste-amis.svg)

![Creer-partie.svg](Creer-partie.svg)



![umlAlex.svg](umlAlex.svg)



## MCD

TODO


More to come... stay tuned 